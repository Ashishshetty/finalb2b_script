    require 'rufus-scheduler'
    require 'rubygems'
    require 'csv'
    require 'date'
    require 'mysql'
    require 'time'
    require 'json'
    require 'logger'
    require 'curb'
    require 'pry'
    require 'mail'
    require 'time'
    require 'open-uri'

    # for the logger just create a folder called log in the same directory where you run the script 
    PYLogger = Logger.new( File.join("./log/" + File.basename(__FILE__) + ".logs"))
    p "here"
    PYLogger.level = Logger::DEBUG

  def data_with_productcode(db_user, db_pass, database_name, db_input_table, db_purl_column, db_output_table, need_seller)
    db_output_table_products = db_output_table + "_products"
    db_output_table_seller = db_output_table + "_seller"
    con = Mysql.new 'localhost', "#{db_user}", "#{db_pass}", "#{database_name}"

    con.query("DROP TABLE IF EXISTS #{db_output_table_products}")

    con.query("CREATE TABLE IF NOT EXISTS \
      #{db_output_table_products}(id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, \
      title_portal VARCHAR(2048), \
      brand_portal VARCHAR(1024), \
      remote_url text, \
      image_url text, \
      seller VARCHAR(1024), \
      mrp_portal VARCHAR(1024), \
      productcode varchar(30), \
      available TINYINT(1), \
      currp VARCHAR(1024),\
      mrp_violation VARCHAR(256),\
      currp_violation VARCHAR(256),\
      created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, \
      updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP )")
    if need_seller
      con.query("DROP TABLE IF EXISTS #{db_output_table_seller}")

      con.query("CREATE TABLE IF NOT EXISTS \
        #{db_output_table_seller}(id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, \
        title VARCHAR(2048), \
        brand VARCHAR(1024), \
        remote_url text, \
        image_url text, \
        seller VARCHAR(1024), \
        mrp VARCHAR(1024), \
        productcode varchar(30), \
        available TINYINT(1), \
        currp VARCHAR(1024),\
        created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, \
        updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP )")
      # con.query("DROP TABLE IF EXISTS #{db_output_table_seller}")
      # con.query("CREATE TABLE IF NOT EXISTS \
      #   #{db_output_table_seller}(id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, \
      #   seller_name VARCHAR(256),\
      #   currP_seller VARCHAR(1024),\
      #   productcode varchar(30), \
      #   created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, \
      #   updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP )")
    end

    res = con.query("SELECT * FROM #{db_input_table}")
    if !res.nil? && res.num_rows != 0
      res.each_hash do |row|        
	sleep(rand(2..5))
        product_url = row["#{db_purl_column}"]
        url = product_url
	puts product_url
        break if product_url.nil?
#	product_url.index("/p/").nil?
        product_url = "https://catalog.paytm.com/v1/p/" + product_url[product_url.index("/p/")+3..-1]
        puts product_url
	http_request = Curl.get(product_url)
        PYLogger.debug("PROD URL IS #{product_url}")
        result = JSON.parse(http_request.body_str)
        File.write("check.json",result)
        productcode = result['parent_id']
        p product_url
        if result['name'].nil?
          title = " "
        else
          title = result['name']
        end
        
        if result['brand'].nil?
          brand = " "
        else
          brand = result['brand']
        end
        
        if result['actual_price'].nil? 
          mrp = " "
        else
          mrp = result['actual_price']
        end
        
        if result['offer_price'].nil?
          currP = " "
        else
          currP = result['offer_price']
        end
        if result['instock'].nil?
          p available = false
        else
          p available = result['instock']
        end
        (available == true)? avail = 1 : avail = 0  
        if result['image_url'].nil?
          image_url = " "
        else
          p image_url = result['image_url']
        end

        if result['pincode'].nil?
          pincode = " "
        else
          p pincode = result['pincode']
        end

        if result['merchant'].nil? 
          seller = " "
        else
          p seller = result['merchant']['merchant_name']
        end
        con.query("INSERT INTO #{db_output_table_products} (title_portal,brand_portal,remote_url,image_url,seller,mrp_portal,productcode,available,currp) VALUES ('#{Mysql.escape_string(title)}','#{Mysql.escape_string(brand)}','#{url}','#{image_url}','#{Mysql.escape_string(seller)}','#{mrp}','#{productcode}','#{avail}','#{currP}')")
        con.query("INSERT INTO #{db_output_table_seller} (title,brand,remote_url,image_url,seller,mrp,productcode,available,currp) VALUES ('#{Mysql.escape_string(title)}','#{Mysql.escape_string(brand)}','#{url}','#{image_url}','#{Mysql.escape_string(seller)}','#{mrp}','#{productcode}','#{avail}','#{currP}')") if need_seller 
        if need_seller and !result['other_sellers'].nil?
          no_sellers = result['other_sellers']['values'].length
          (0..no_sellers-1).each do |i|
            if result['other_sellers']['values'][i]['name'].nil?
              seller_name = " "
            else
              p seller_name = result['other_sellers']['values'][i]['name']
            end
            if result['other_sellers']['values'][i]['offer_price'].nil?
              offer_price = " "
            else
              p offer_price = result['other_sellers']['values'][i]['offer_price']
            end
            if result['other_sellers']['values'][i]['url'].nil? 
              product_url = " "
            else
              p product_url = result['other_sellers']['values'][i]['url']
            end
            p query = "INSERT INTO #{db_output_table_seller} (title,brand,remote_url,image_url,seller,mrp,productcode,available,currp) VALUES ('#{Mysql.escape_string(title)}','#{Mysql.escape_string(brand)}','#{url}','#{image_url}','#{Mysql.escape_string(seller_name)}','#{mrp}','#{productcode}','#{avail}','#{offer_price}')"
            
            con.query(query)
          end
        end

      end
    end

    PYLogger.debug("DONE!!!!!!!!!!!!!!!!!!!!!!!!!!!!")

  end
# scheduler = Rufus::Scheduler.new
# scheduler.cron '00 00 * * *' do
# scheduler.cron '50 07 * * *' do
  p "here"
  need_seller = 0
  output_table = "Paytm_LG"
  input_table = "input_paytm"

  data_with_productcode("root", "123", "LG", input_table, "url", output_table, need_seller)
  
  output_table_products = output_table + "_products"
  db_output_table_seller = output_table + "_seller"
  # final_py_output = "Output_paytm_data"
# data_with_productcode("root", "123", "LG", input_table, "url", output_table_products, need_seller)

@dbHandle = Mysql.new("localhost","root","123","LG")

@dbHandle.query("drop table if exists Output_paytm_lg_data")
@dbHandle.query("create table Output_paytm_lg_data as(
SELECT *, (case when ((currp < .93*input_paytm.mrp) OR (currp > mrp)) then 'Yes' else 'No' end) as price_violation, input_paytm.mrp*93/100 as MOP FROM Paytm_LG_products
LEFT JOIN input_paytm
ON Paytm_LG_products.remote_url = input_paytm.url)")

results_series = @dbHandle.query("select title,title_portal, mrp_portal, currp, seller,productcode,remote_url, image_url,category,brand_portal,sku,source,available,price_violation,mrp,mop from Output_paytm_lg_data ")

CSV.open("/home/ubuntu/Paytm/csv/paytm_lg#{Date.today.to_s}.csv","wb") do |csv|
  csv << ["Product Name","Title","MRP_Portal", "Price","Seller Name","Product Code", "Link", "Product Image","Category", "Brand","SKU","Portal Name","Availability","Price Violation","MRP_LG","MOP"]
  results_series.each_hash do |s1|
    csv << ["#{s1['title']}","#{s1['title_portal']}","#{s1['mrp_portal']}", "#{s1['currp']}", "#{s1['seller']}","#{s1['productcode']}","#{s1['remote_url']}","#{s1['image_url']}","#{s1['category']}","#{s1['brand_portal']}","#{s1['sku']}","#{s1['source']}","#{s1['available']}","#{s1['price_violation']}","#{s1['mrp']}","#{s1['mop']}",]
  end
end

Mail.defaults do
    delivery_method :smtp,  {
    :address => 'smtp.gmail.com',
    :port => '25',
    :user_name => 'Insights@intellolabs.com',
    :password => 'intellolabs@123',
    :enable_starttls_auto => true}
end

@mail = Mail.new do
      from 'Insights@intellolabs.com'
        to 'sujith@comparedapp.com'
#        cc 'himani@comparedapp.com,ashish@comparedapp.com,roli@comparedapp.com,nishantm@comparedapp.com,sudeepa.shetty@comparedapp.com,adit@comparedapp.com'
      subject 'Paytm LG'
      body 'Hi all
     Attaching csv file of Paytm lg '
end
x=Date.today.to_s
@mail.add_file("/home/ubuntu/Paytm/csv/paytm_lg"+x+".csv")
@mail.deliver!


#end
# scheduler.join

