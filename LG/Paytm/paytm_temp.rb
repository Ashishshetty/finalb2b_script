require 'rufus-scheduler'
require 'rubygems'
require 'csv'
require 'date'
require 'mysql'
require 'time'
require 'json'
require 'logger'
require 'curb'
require 'pry'
require 'mail'
require 'time'
require 'open-uri'
require 'nokogiri'


db_user =  "root"
db_pass = "123"
database_name = "LG"
db_output_table_products = "paytm_lg_output"
$con = Mysql.new 'localhost', "#{db_user}", "#{db_pass}", "#{database_name}"

def page_curl(url,row)
p title = row['title_portal']
p brand = row['brand_portal']
p remote_url = row['remote_url']
p image_url = row['image_url']
p productcode = row['productcode']


  `curl -L "#{url}" > paytm.html`

  doc = Nokogiri::HTML(open("paytm.html"))
  p mrp = !doc.css('[itemprop="price"]').empty? ? doc.css('[itemprop="price"]').text.strip.gsub!(/[^0-9]/, '').to_i : '0'
  p selling_price = !doc.css('._2CkE._30zs').empty? ? doc.css('._2CkE._30zs').text.strip.gsub!(/[^0-9]/, '').to_i : '0'
  if !doc.css('._12d4').empty?
    if doc.css('._12d4').text.downcase.include? "out of"
      p available = '0' 
    else 
      p available = '1'
    end
  end

  if doc.css('._12d4').empty?
   puts 'inside no div condition'
   p available = '1'
 end
 p sellername = !doc.css('._1REL').empty? ? doc.css('._1REL').text : ''
 # $con.query("INSERT INTO paytm_lg_output (remote_url,seller,mrp_portal,available,currp) VALUES ('#{url}','#{sellername}','#{mrp}','#{available}','#{selling_price}')")
 $con.query("INSERT INTO paytm_lg_output (title,brand,remote_url,image_url,seller,mrp_portal,productcode,available,currp) VALUES ('#{Mysql.escape_string(title)}','#{Mysql.escape_string(brand)}','#{url}','#{image_url}','#{Mysql.escape_string(sellername)}','#{mrp}','#{productcode}','#{available}','#{selling_price}')")
end

$con.query("DROP TABLE IF EXISTS paytm_lg_output")

$con.query("CREATE TABLE IF NOT EXISTS \
  paytm_lg_output(id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, \
  title VARCHAR(2048), \
  brand VARCHAR(1024), \
  remote_url text, \
  image_url text, \
  seller VARCHAR(1024), \
  mrp_portal VARCHAR(1024), \
  productcode varchar(30), \
  available TINYINT(1), \
  currp VARCHAR(1024),\
  created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, \
  updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP )")

res = $con.query("SELECT * FROM Paytm_LG_products")

if !res.nil? && res.num_rows != 0
  res.each_hash do |row|
    url = row['remote_url']
    p url
    page_curl(url,row)
  end
end
# 1680
$con.close if $con

@dbHandle = Mysql.new("localhost","root","#{db_pass}","#{database_name}")

@dbHandle.query("drop table if exists Output_paytm_lg_data")
@dbHandle.query("create table Output_paytm_lg_data as(
SELECT *, (case when ((currp < .93*input_paytm.MRP) OR (currp > MRP)) then 'Yes' else 'No' end) as price_violation, input_paytm.MRP*93/100 as MOP FROM paytm_lg_output
LEFT JOIN input_paytm
ON paytm_lg_output.remote_url = input_paytm.url)")

results_series = @dbHandle.query("select title,title_portal, mrp_portal, currp, seller,productcode,remote_url, image_url,category,brand_portal,sku,source,available,price_violation,mrp,mop from Output_paytm_lg_data ")

CSV.open("/home/ubuntu/Paytm/csv/paytm_lg#{Date.today.to_s}.csv","wb") do |csv|
  csv << ["Product Name","Title","MRP_Portal", "Price","Seller Name","Product Code", "Link", "Product Image","Category", "Brand","SKU","Portal Name","Availability","Price Violation","MRP_LG","MOP"]
  results_series.each_hash do |s1|
    csv << ["#{s1['title']}","#{s1['title_portal']}","#{s1['mrp_portal']}", "#{s1['currp']}", "#{s1['seller']}","#{s1['productcode']}","#{s1['remote_url']}","#{s1['image_url']}","#{s1['category']}","#{s1['brand_portal']}","#{s1['sku']}","#{s1['source']}","#{s1['available']}","#{s1['price_violation']}","#{s1['mrp']}","#{s1['mop']}",]
  end
end

Mail.defaults do
    delivery_method :smtp,  {
    :address => 'smtp.gmail.com',
    :port => '25',
    :user_name => 'Insights@intellolabs.com',
    :password => 'intellolabs@123',
    :enable_starttls_auto => true}
end

@mail = Mail.new do
      from 'Insights@intellolabs.com'
       to 'sujith@comparedapp.com,lokesh@intellolabs.com'
       cc 'himani@comparedapp.com,ashish@comparedapp.com,roli@comparedapp.com,nishantm@comparedapp.com,sudeepa.shetty@comparedapp.com,adit@comparedapp.com'
      subject 'Paytm LG'
      body 'Hi all
     Attaching csv file of Paytm lg '
end
x=Date.today.to_s
@mail.add_file("/home/ubuntu/Paytm/csv/paytm_lg"+x+".csv")
@mail.deliver!

