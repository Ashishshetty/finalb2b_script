require 'rubygems'
require 'csv'
require 'date'
require 'mysql'
require 'nokogiri'
require 'json'
require 'logger'
require 'watir-webdriver'
require 'headless'
require 'pry'

con = Mysql.new 'localhost','root','123','LG'
con.query("DROP TABLE IF EXISTS lg_seller")
con.query("CREATE TABLE IF NOT EXISTS \
                   lg_seller(id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, \
                   sellername VARCHAR(255), \
                   sellerprice VARCHAR(255), \
                   mrp VARCHAR(256), \
                   productcode VARCHAR(1024), \
                   created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, \
                   updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
                   )")

res = con.query("SELECT * FROM flipkart_input")

  headless = Headless.new
  headless.start
  res.each_hash do |row|
    browser = Watir::Browser.new
    productcode = row["productnumber"]
    # productcode = url[url.index('?pid=') + 5..-1].split('&').first
    browser.goto "https://www.flipkart.com/sellers?pid=#{productcode}&otracker=CLP_filters&fetchId=d5eae5f1-dce7-48e3-8ba0-379d80d22410.#{productcode}"
    source = browser.html

    if File.exist?('myfile.html')
      `rm -rf myfile.html`
    end

    open('myfile.html', 'a') do |f|
      f << source
    end

    doc = Nokogiri::HTML(open("myfile.html"))
    # puts product_url = url
    puts product_id = productcode

    div_count = doc.css('._2VDj-t ._3pZJne').count

    0.upto(0) do |i|

      puts seller_name = doc.css('._2VDj-t ._3pZJne a')[i].text

      if doc.css('._2VDj-t ._3pZJne ._1vC4OE')[i].nil?
        seller_price = doc.css('._2VDj-t ._3pZJne ._1vC4OE')[i].text
        seller_price = seller_price.gsub!(/[^0-9A-Za-z]/, '')
      else
        sellerprice = "0"
      end

      if !doc.css('._2VDj-t ._3pZJne ._3auQ3N')[i].nil?
        mrp = doc.css('._2VDj-t ._3pZJne ._3auQ3N')[i].text
        mrp = mrp.gsub!(/[^0-9A-Za-z]/, '')
      else
        mrp = "0"
      end

      insert = "INSERT INTO lg_seller ( productcode, sellername, sellerprice, mrp) VALUES ('#{product_id}', '#{seller_name}', '#{seller_price}', '#{mrp}')"
      con.query(insert)
    end

    browser.close
  end

  headless.destroy
