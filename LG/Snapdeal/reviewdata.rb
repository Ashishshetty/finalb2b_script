require 'rubygems'
require 'csv'
require 'date'
require 'mysql'
require 'time'
require 'json'
require 'logger'
require 'curb'
require 'mail'
require 'open-uri'
require 'nokogiri'
require 'pry'
require 'rufus-scheduler'

# scheduler = Rufus::Scheduler.new
# scheduler.every '2d', first_in: Time.now + 2*60*60 do

def data_reviews(db_user, db_pass, database_name,db_input_table , db_output_table)
   con = Mysql.new 'localhost', "#{db_user}", "#{db_pass}", "#{database_name}"
   con.query("DROP TABLE IF EXISTS #{db_output_table}")

    con.query("CREATE TABLE IF NOT EXISTS \
        #{db_output_table}(id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, \
        title VARCHAR(2048),\
        brand VARCHAR(1024), \
        productcode VARCHAR(1024), \
        reviewername VARCHAR(1024), \
        reviewed_at VARCHAR(1024), \
        verified_buyer VARCHAR(1024), \
        reviewrating VARCHAR(1024), \
        review text)")

    res = con.query("select * from #{db_input_table}")

    va = 1
    puts res.num_rows
    res.each_hash do |row|
      
        puts brand = "ucb"
        puts "HERE WE ARE #{va}"
        va = va + 1
        productcode = row['productnumber'].strip
        next if productcode.to_s.empty?
        puts "kickstart"
        pg_number = 1
        for pg_number in 1..30
            retrycount = 3

            begin
                p "https://www.snapdeal.com/product/#{productcode}/reviews/ajax?sortBy=RECENCY&page=#{pg_number}"
                file = Nokogiri::HTML(open("https://www.snapdeal.com/product/#{productcode}/reviews/ajax?sortBy=RECENCY&page=#{pg_number}"))
            rescue
                sleep(rand(1..2))
                next if retrycount == 0
                retrycount -= 1
                retry
            end
            reviewcount = 0
            
            for i in 0..9
                if file.css(".user-review")[i] != nil
                    reviewrating = file.css(".user-review")[i].css(".rating .sd-icon.sd-icon-star.active").count
                else
                    reviewrating = ""
                end
                title = ""
                title = file.css('.user-review .head')[i].text if !file.css('.user-review .head')[i].nil?
                temp =  ""
                reviewername = ""
                reviewed_at = ""
                year = nil
                temp=file.css('.user-review ._reviewUserName')[i].text if !file.css('.user-review ._reviewUserName')[i].nil?
                reviewername = temp.split('on')[0].split('by')[1].strip if !temp.empty?
                reviewed_at = temp.split('on')[1].strip if !temp.empty?
                # year = reviewed_at.split(",").last.to_i if !reviewed_at.nil?
                # break if year < 2017 and !year.nil?
                verified_buyer = ""
                verified_buyer = file.css('.user-review .verifiedname')[i].text if !file.css('.user-review .verifiedname')[i].nil?
                review = ""
                review = file.css('.user-review p')[i].text if !file.css('.user-review p')[i].nil?
                puts "#{productcode}"
                puts "#{title}"
                puts "#{reviewername}"
                puts "#{reviewed_at}"
                puts "#{verified_buyer}"
                puts "#{review}"
                reviewcount +=1 if review!="" or reviewername!=""
                if review!="" or reviewername!=""
                    con.query("INSERT INTO #{db_output_table} (title,brand,productcode,reviewername,reviewed_at,verified_buyer,reviewrating,review) VALUES ('#{Mysql.escape_string(title)}','#{Mysql.escape_string(brand)}','#{Mysql.escape_string(productcode)}','#{Mysql.escape_string(reviewername)}','#{Mysql.escape_string(reviewed_at)}','#{Mysql.escape_string(verified_buyer)}','#{reviewrating}','#{Mysql.escape_string(review)}')")
                end
            end
                if reviewcount!=10
                    break
                end
            end

        end

ensure
  con.close if con
end

def createcsv(db_user, db_pass,db_name,db_reviewdata_output_table)

    con = Mysql.new 'localhost', "#{db_user}", "#{db_pass}", "#{db_name}"
    puts filename = db_reviewdata_output_table+".csv"
    if File.exist?("#{filename}")
      `rm -rf #{filename}`
    end
    csv = CSV.open("#{filename}","ab")
    csv << ["Title","Productcode","Reviewername","review date","Rating","Review"]

    res = con.query("select * from #{db_reviewdata_output_table}")
    
    res.each_hash do |row| 
        csv << [row["title"],row["productcode"],row["reviewername"],row["reviewed_at"],row["reviewrating"],row["review"]]
    end
end

db_name = "LG"
db_user = "root"
db_pass = "root"
db_productdata_output_table = "snapdeal_input"
db_reviewdata_output_table = "snapdeal_reviews"

data_reviews(db_user, db_pass,db_name,db_productdata_output_table , db_reviewdata_output_table)
createcsv(db_user, db_pass,db_name,db_reviewdata_output_table)  
# sleep(240)
# Mail.defaults do
#     delivery_method :smtp,  {
#     :address => 'smtp.gmail.com',
#     :port => '25',
#     :user_name => 'Insights@intellolabs.com',
#     :password => 'intellolabs@123',
#     :enable_starttls_auto => true}
# end

# @mail = Mail.new do
#       from 'Insights@intellolabs.com'
#         to 'nishantm@comparedapp.com'
#         cc 'roli@comparedapp.com, ashish@comparedapp.com,sujith@comparedapp.com,nikhil@comparedapp.com, adit@comparedapp.com,sudeepa.shetty@comparedapp.com,lokesh@intellolabs.com'

#    subject 'ucb 30 days snapdeal reviewdata'
#       body 'Hi all
#      Attaching csv file of ucb 30 days snapdeal reviewdata'

# end
# x=Date.today.to_s
# @mail.add_file("snapdeal_ucb_30days_reviewdata.csv")  ############# write ur file name if you are attaching any file
# @mail.deliver!


# end
#    scheduler.join
