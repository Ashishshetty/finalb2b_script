    require 'rufus-scheduler'
    require 'rubygems'
    require 'csv'
    require 'date'
    require 'mysql'
    require 'time'
    require 'json'
    require 'logger'
    require 'curb'
    require 'mail'
    require 'pry'
    require 'open-uri'
    require 'nokogiri'

   scheduler = Rufus::Scheduler.new
   scheduler.cron '40 17 * * *' do
SDlogger = Logger.new( File.join("./log/" + File.basename(__FILE__) + ".logs"))
SDlogger.level= Logger::DEBUG
SDlogger.debug("STARTING NOW")

def getSD_pagecurl(con, productcode, db_output_table)
  retry_pagecurl = 4
  url = "https://www.snapdeal.com/product/lg-255-glq282ssam-multi-air/#{productcode}"
  while retry_pagecurl > 0
    begin
      `curl -L "#{url}" > sd_page.html`     
      file = Nokogiri::HTML(open("sd_page.html"))
      break
    rescue Exception => e
      retry_pagecurl = retry_pagecurl -1
    end
  end
  if retry_pagecurl == 0
    return
  end

  doc = file
  return if doc.nil?
  return if doc.title.downcase.include? "error" 
  if !doc.css('link[rel=canonical]').nil?
    remote_url = doc.css('link[rel=canonical]').attr('href').value  if remote_url = !doc.css('link[rel=canonical]').attr('href').nil?
   else
    remote_url = url
  end
  title = doc.css('h1').text.strip 
  currprice = doc.css(".payBlkBig").text
  currprice = currprice.gsub(/[^\d]/,"")
  mrp = doc.css(".pdpCutPrice").text
  mrp = mrp.gsub(/[^\d]/,"")
  doc.css(".row.marL0").empty??  available = '0' : available = "1"
  ratings = doc.css(".total-rating.showRatingTooltip").text.to_i
  reviews = doc.css(".numbr-review").text.to_i
  avg_rating = doc.css(".avrg-rating").text
  avg_rating = avg_rating.gsub(/[^\d.]/,"").to_f
  if !doc.css(".pdp-e-seller-info-name")[0].nil?
    seller = doc.css(".pdp-e-seller-info-name")[0].text
    seller = seller.strip if !seller.nil?
  else
    seller = ""
  end
  image = doc.css("#bx-slider-left-image-panel li img")[0].attr("src") if !doc.css("#bx-slider-left-image-panel li img")[0].nil?
  brand_temp = ""
  brand_temp = doc.css(".greyText")[0].parent if !doc.css(".greyText")[0].nil?
  if brand_temp.class != String
    brand = brand_temp.css("a").text 
  else
    brand = ""
  end
  # binding.pry
  p "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
  p title
  p brand
  p remote_url
  p image
  p mrp = mrp.to_f
  p productcode
  p available
  p currprice = currprice.to_f
  p "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
  p "Insert into #{db_output_table} (title_portal,brand_portal,remote_url,image,mrp,productcode,available,currp) values ('#{title}','#{brand}','#{remote_url}','#{image}','#{mrp}','#{productcode}','#{available}','#{currprice}')"
  con.query("Insert into #{db_output_table} (title_portal,brand_portal,remote_url,image,mrp,productcode,available,currp) values ('#{title}','#{brand}','#{remote_url}','#{image}','#{mrp}','#{productcode}','#{available}','#{currprice}')")

end




db_output_table = "SD_lg_data"
con = Mysql.new 'localhost', 'root', '123', 'LG'



con.query("DROP TABLE IF EXISTS #{db_output_table}")
con.query("CREATE TABLE IF NOT EXISTS \
  #{db_output_table}(id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, \
  title_portal VARCHAR(2048), \
  brand_portal VARCHAR(1024), \
  remote_url text, \
  image text, \
  mrp float, \
  cat_disc text, \
  productcode varchar(30), \
  available TINYINT(1), \
  created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, \
  updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP , \
  currp float)")


res = con.query("SELECT distinct productnumber FROM snapdeal_input")


if !res.nil? && res.num_rows != 0

 res.each_hash do |row|
   product_id = row['productnumber'].strip
   SDlogger.debug("productcode  #{product_id}")
   retrycount = 4
   while(retrycount > 0 )
    begin
      getSD_pagecurl(con, product_id, db_output_table)
      sleep(3)
      break
    rescue Exception => e
      SDlogger.error("productcode #{product_id} error")
      sleep(rand(2..5))
      p "error"
      retrycount = retrycount - 1
    end
   end

   if retrycount == 0
    next  
   end
  end
end

SDlogger.debug("Done with product data SD")

  #**********************************************************************************
  #####################      Now fetching sellers          ###########################
  #***********************************************************************************

  con = Mysql.new 'localhost', 'root', '123', 'LG'
  con.query("DROP TABLE IF EXISTS #{db_output_table}SELLER")

  con.query("CREATE TABLE IF NOT EXISTS \
    #{db_output_table}SELLER(
    seller VARCHAR(1024), \
    productid varchar(30))")

  res = con.query("SELECT * FROM snapdeal_input")
  if !res.nil? && res.num_rows != 0
    res.each_hash do |row|
      product_code=row['productnumber'].strip
      retry_count = 10
      begin 
        file = Nokogiri::HTML(open("https://www.snapdeal.com/viewAllSellers/product/lg-43lh600t-108-cm-43/#{product_code}","User-Agent" => "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36"))
      rescue
       sleep(rand(3..5))
       retry_count -=1
       retry if retry_count !=0
       next if retry_count == 0
     end    

     for i in 0..0
      seller=""
      seller=file.css('.seller-nm')[i].text.gsub("\t", "").gsub("\r","").gsub("\n","") if !file.css('.seller-nm')[i].nil?
      productid =product_code.to_s
      # puts "#{seller}"
      # puts productid
      con.query("INSERT INTO #{db_output_table}SELLER (seller,productid) VALUES ('#{Mysql.escape_string(seller)}','#{Mysql.escape_string(productid)}')")

    end
  end
  con.close if con
end
#************************************************************************************
################### Now mapping seller table with data table #########################

@dbHandle = Mysql.new("localhost","root","123","LG")
@dbHandle.query("drop table if exists Output_Snapdeal_lg")
@dbHandle.query("create table Output_Snapdeal_lg as( SELECT * FROM #{db_output_table}
  LEFT JOIN #{db_output_table}SELLER
  ON #{db_output_table}.productcode = #{db_output_table}SELLER.productid)")

#**********************************************************************
###### For removing duplication we are doing distinct #################

# @dbHandle.query("drop table if exists Output_Snapdeal_lg_data")
# @dbHandle.query("create table Output_Snapdeal_lg_data (select distinct title_portal,brand_portal,remote_url,image,mrp,cat_disc,productcode,available,currp,seller from Output_Snapdeal_lg)")

# @dbHandle.query("ALTER TABLE Output_Snapdeal_lg_data MODIFY mrp FLOAT")
# @dbHandle.query("ALTER TABLE Output_Snapdeal_lg_data MODIFY currp FLOAT")




#************************************************************************************************************

@dbHandle.query("drop table if exists Output_snapdeal")
@dbHandle.query("create table Output_snapdeal as (
  SELECT * , (case when ((currp < .93*snapdeal_input.mrp_lg) OR (currp > mrp_lg) ) then 'Yes' else 'No' end) as price_violation FROM Output_Snapdeal_lg
  LEFT JOIN snapdeal_input
  ON Output_Snapdeal_lg.productid = snapdeal_input.productnumber)")

@dbHandle.query("ALTER TABLE Output_snapdeal Add Column mop Float")

@dbHandle.query("SET SQL_SAFE_UPDATES=0")
@dbHandle.query('Update Output_snapdeal SET mop = mrp_lg*93/100')
@dbHandle.query("SET SQL_SAFE_UPDATES=1")


#*************************************************************************

results_series = @dbHandle.query("select title,title_portal, mrp, currp, seller,productcode,remote_url, image,category,brand,sku,source,available,price_violation,mrp_lg,mop from Output_snapdeal ")

filename = "/home/ubuntu/Snapdeal/csv/snapdeal_lg#{Date.today.to_s}.csv"
# filename = "snapdeal_test.csv"
CSV.open("#{filename}","wb") do |csv|
  csv << ["Product Name","Title","MRP_Portal", "Price","Seller Name","Product Code", "Link", "Product Image","Category", "Brand","SKU","Portal Name","Availability","Price Violation","MRP_LG","MOP"]
  results_series.each_hash do |s1|
    csv << ["#{s1['title']}","#{s1['title_portal']}","#{s1['mrp']}", "#{s1['currp']}", "#{s1['seller']}","#{s1['productcode']}","#{s1['remote_url']}","#{s1['image']}","#{s1['category']}","#{s1['brand']}","#{s1['sku']}","#{s1['source']}","#{s1['available']}","#{s1['price_violation']}","#{s1['mrp_lg']}","#{s1['mop']}",]
  end
end
#for sending mail with attachment**************************************************************************************************
Mail.defaults do
  delivery_method :smtp,  {
    :address => 'smtp.gmail.com',
    :port => '25',
    :user_name => 'Insights@intellolabs.com',
    :password => 'intellolabs@123',
    :enable_starttls_auto => true}
  end

  @mail = Mail.new do
    from 'Insights@intellolabs.com'
        #to 'sujith@comparedapp.com'
        to 'himani@comparedapp.com'
        cc 'roli@comparedapp.com,sudeepa.shetty@comparedapp.com,ashish@comparedapp.com,adit@comparedapp.com,dev@intellolabs.com,sujith@comparedapp.com'
        
        subject 'Snapdeal LG'
        body 'Hi all 
        Attaching csv file of snapdeal lg '
        
      end
      x=Date.today.to_s
      @mail.add_file("#{filename}")
      @mail.deliver!



end
  scheduler.join
