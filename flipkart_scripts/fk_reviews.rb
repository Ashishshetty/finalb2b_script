	require 'mysql'
	require 'pry'
	require 'json'

	def get_pages_count(productcode,start,url, con)
		p "Inside"
		pages = 0
		retry_count = 3
		url = "https://www.flipkart.com/api/3/product/reviews?sortOrder=MOST_RECENT&count=10&start=#{start}&reviewerType=ALL&ratings=ALL&productId=#{productcode}"
		puts '============='
		puts start
		puts '============='
		while(retry_count > 0)
			begin
				http_request = `curl  -H "X-User-Agent:Mozilla/5.0 (Linux; Android 6.0; XT1068 Build/MPB24.65-34; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/53.0.2785.124 Mobile Safari/537.36 FKUA/Retail/731000/Android/Mobile (motorola/XT1068/7d9ca765681425a3aff846e8d7af29b8)" -H "Device-Id:7d9ca765681425a3aff846e8d7af29b8" -H "sn:2.VI262807C674CB4471BFC9F5198D91CD13.SIC09E6E846DEF461E91693B4327B17363.VSF7DD1F41B11A4BAFBBEAB75AF2C2A63E.1476104437 " -H "Browser-Name:Chrome" -H "secureToken:KYxjbsQEZHGqUHhpkKP4UEEXKJ2EQUFnwsY041zvo/xI3kistp+tIBmnIC/nlx4iM3A7R8VkQYAElMQdueK6PQ==" -H "User-Agent:Mozilla/5.0 (Linux; Android 6.0; XT1068 Build/MPB24.65-34; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/53.0.2785.124 Mobile Safari/537.36 FKUA/Retail/731000/Android/Mobile (motorola/XT1068/7d9ca765681425a3aff846e8d7af29b8)" -H "X-Visit-Id:7d9ca765681425a3aff846e8d7af29b8-1476086203898" -H "Connection:Keep-Alive" "#{url}"`
				result = JSON.parse(http_request)
				break
			rescue JSON::ParserError => e
				puts "Json parse error"
				retry_count = retry_count - 1 
			rescue
				retry_count = retry_count - 1
				sleep(2)

				p "Error getting the reviews count"
			end
			if retry_count == 0 
				con.query("insert into fk_reviews (productcode) values (productcode)")
			end
		end


		if !result.nil? && !result['RESPONSE'].nil? && !result['RESPONSE']['params'].nil? && !result['RESPONSE']['params']['totalCount'].nil?
			p pages = result['RESPONSE']['params']['totalCount']

			round = pages % 10
			a = 10 - round
			if a != 10
				pages = pages + a
			end

			(0..9).each do |page|
				if !result['RESPONSE']['data'][page].nil?
					reviews_page = result['RESPONSE']['data'][page]
					p review = result['RESPONSE']['data'][page]['value']['text']
					p rating = result['RESPONSE']['data'][page]['value']['rating']
					p au_name = result['RESPONSE']['data'][page]['value']['author']
					p title = result['RESPONSE']['data'][page]['value']['title']
					p date = result['RESPONSE']['data'][page]['value']['created']
					p url
					p productcode
					p "here"
					review = "" if review.nil?
					title = "" if title.nil?
					return if date.split(",").last.to_i < 2016

					puts "Insert into fk_reviews (title, productcode, review, rating, reviewer, review_date) values ('#{title}','#{productcode}', '#{Mysql.escape_string(review)}', '#{rating}', '#{au_name}', '#{date}')"
					con.query("Insert into fk_reviews (title, productcode, review, rating, reviewer, review_date) values ('#{Mysql.escape_string(title)}','#{productcode}', '#{Mysql.escape_string(review)}', '#{rating}', '#{au_name}', '#{date}')")
				end
			end

			start = start + 10
			if start <= pages
				get_pages_count(productcode,start,url, con)
			end
		end
	end


	con = Mysql.new 'localhost', 'root', 'root', 'loreal'
    res = con.query("SELECT distinct (productcode) FROM flipkart_first_input")
    
    # con.query("Drop table if exists fk_reviews")
      con.query("CREATE TABLE IF NOT EXISTS \
        fk_reviews(id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, \
        title VARCHAR(2048), \
        productcode text, \
        review text, \ 
        rating text, \
        reviewer text, \
        review_date text, \
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP)")


    if !res.nil? && res.num_rows != 0
	      res.each_hash do |row|

	      	productcode = row['productcode']
	      	start = 0
			url = "https://www.flipkart.com/api/3/product/reviews?sortOrder=MOST_RECENT&count=10&start=#{start}&reviewerType=ALL&ratings=ALL&productId=#{productcode}"

			get_pages_count(productcode, start, url , con)

	      end
    end
