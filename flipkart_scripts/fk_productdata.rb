    require 'rufus-scheduler'
    require 'rubygems'
    require 'csv'
    require 'pry'
    require 'date'
    require 'mysql'
    require 'time'
    require 'json'
    require 'logger'
    require 'curb'
    require 'mail'
    require 'nokogiri'
    require 'open-uri'


    con = Mysql.new 'localhost', 'root', 'root', 'loreal'
    res = con.query("SELECT * FROM fk_input")
    if !res.nil? && res.num_rows != 0
      linecount = 0

      affid="nishantfi"
      afftoken="fd7676f2496144209c3a01fe0008f207"

      con.query("Drop table if exists flipkart_lg_data")
      con.query("CREATE TABLE IF NOT EXISTS \
        flipkart_lg_data(id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, \
        title_portal VARCHAR(2048), \
        remote_url VARCHAR(1024), \
        brand_portal VARCHAR(1024), \
        available VARCHAR(1024), \
        strikePrice FLOAT, \
        lowestPrice FLOAT, \
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, \
        productcode VARCHAR(512), \
        discount VARCHAR(255), \
        hierarchy text, \
        extimgpath text, \
        attributes text, \
        offer VARCHAR(1024), \
        keyspecs text)")

      FKlogger = Logger.new( File.join("./log/" + File.basename(__FILE__) + ".logs"))

      FKlogger.level = Logger::DEBUG

      res.each_hash do |row|
        linecount +=1;
        rowTimeStamp = row['productcode'].strip
        retry_curl = 3
        uri = "https://affiliate-api.flipkart.net/affiliate/1.0/product.json?id=#{rowTimeStamp}"
        begin          
          resp = `curl -H Fk-Affiliate-Id:#{affid} -H Fk-Affiliate-Token:#{afftoken} https://affiliate-api.flipkart.net/affiliate/1.0/product.json?id=#{rowTimeStamp}`
        rescue
         FKlogger.error("Error Curl action for product code #{rowTimeStamp}")
         retry if retry_curl >0

         retry_curl -=1
         next if retry_curl == 0
       end
       if !resp.nil? && resp.index("I should not be").nil? && resp.length > 0
        strData = JSON.parse(resp)
        productid= strData['productBaseInfoV1']['productId']
        title= strData['productBaseInfoV1']['title']
        image = strData['productBaseInfoV1']['imageUrls']['400x400']
        mrp  = strData['productBaseInfoV1']['maximumRetailPrice']['amount']
        currp = strData['productBaseInfoV1']['flipkartSellingPrice']['amount']
        remote_url = strData['productBaseInfoV1']['productUrl']
        brand = strData['productBaseInfoV1']['productBrand']
        available = strData['productBaseInfoV1']['inStock']
        discount = strData['productBaseInfoV1']['discountPercentage']
        offers =  strData['productBaseInfoV1']['offers']
        hierarchy = strData['productBaseInfoV1']['categoryPath']
        attributes = strData['productBaseInfoV1']['attributes']

        keyspecs = strData['categorySpecificInfoV1']['keySpecs']

        if image.nil?
         image = strData['productBaseInfoV1']['imageUrls']['200x200']
       end
       if image.nil?
        image=""
      end

      con.query("INSERT INTO flipkart_lg_data \
        (title_portal, remote_url, brand_portal, available, strikePrice, lowestPrice, productcode, discount, hierarchy, extimgpath, attributes, offer, keyspecs) \
        VALUES ('#{Mysql.escape_string(title)}','#{Mysql.escape_string(remote_url)}','#{Mysql.escape_string(brand)}',#{available},#{mrp},#{currp},'#{productid}',#{discount},'#{hierarchy}','#{Mysql.escape_string(image)}','#{attributes}','','#{keyspecs}')")

    else
    end
  end
end


# con = Mysql.new 'localhost','root','123','LG'
# con.query("DROP TABLE IF EXISTS fk_seller")
# con.query("CREATE TABLE IF NOT EXISTS \
#  fk_seller(
#  sellername VARCHAR(255), \
#  productid VARCHAR(1024))")

# res = con.query("SELECT * FROM flipkart_input")

# res.each_hash do |row|
#   product = row['productnumber'].strip
#   url = "https://www.flipkart.com/api/3/page/dynamic/product-sellers?http-method=POST&http-body=%7B%22requestContext%22%3A%7B%22productId%22%3A%22#{product}%22%2C%22listingId%22%3A%22LSTMOBEMZD4KHRF5VZX7FNU5S%22%7D%2C%22locationContext%22%3A%7B%22pincode%22%3A%22%22%7D%7D"
#   http_request = `curl  -H "X-User-Agent:Mozilla/5.0 (Linux; Android 6.0; XT1068 Build/MPB24.65-34; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/53.0.2785.124 Mobile Safari/537.36 FKUA/Retail/731000/Android/Mobile (motorola/XT1068/7d9ca765681425a3aff846e8d7af29b8)" -H "Device-Id:7d9ca765681425a3aff846e8d7af29b8" -H "nsid:2.SI71F27B238DF642AC918D929AD8883FC3.1492498648.VIDA3DEEDC406A463A91B3F649219D98EE" -H "vid:2.VIDA3DEEDC406A463A91B3F649219D98EE.1492498648.VS128043B66ABD4EB188FA1744F015FF97" -H "sn:2.VI262807C674CB4471BFC9F5198D91CD13.SIC09E6E846DEF461E91693B4327B17363.VSF7DD1F41B11A4BAFBBEAB75AF2C2A63E.1476104437 " -H "Browser-Name:Chrome" -H "secureToken:KYxjbsQEZHGqUHhpkKP4UEEXKJ2EQUFnwsY041zvo/xI3kistp+tIBmnIC/nlx4iM3A7R8VkQYAElMQdueK6PQ==" -H "User-Agent:Mozilla/5.0 (Linux; Android 6.0; XT1068 Build/MPB24.65-34; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/53.0.2785.124 Mobile Safari/537.36 FKUA/Retail/731000/Android/Mobile (motorola/XT1068/7d9ca765681425a3aff846e8d7af29b8)" -H "X-Visit-Id:7d9ca765681425a3aff846e8d7af29b8-1476086203898" -H "Connection:Keep-Alive" "#{url}"`
#   result = JSON.parse(http_request)
#   binding.pry
#   result['RESPONSE']['data']['product_seller_detail_1'].each do |a|
#     if a[0] == 'data'
#       i = 0
#       until a[1][i] == nil
#         p seller_name = a[1][i]['value']['sellerInfo']['value']['name']
#         con.query("Insert into fk_seller (sellername,productid) values ('', '#{product}')")

#         if a[1][i]['value']['sellerInfo']['value']['newSeller'] != true
#           p seller_rating = a[1][i]['value']['metadata']['averageRating']
#         else
#           p rating = ""
#         end
#         p seller_price = a[1][i]['value']['metadata']['price']
#         if a[1][i]['value']['pricing']['value']['prices'][0]['strikeOff'] == true
#           p seller_mrp  = a[1][i]['value']['pricing']['value']['prices'][0]['value']
#         else 
#           seller_mrp = seller_price 
#         end
#         break
#       end
#     end
#   end
# end
# con.close if con

# @dbHandle = Mysql.new("localhost","root","123","LG")
# @dbHandle.query("drop table if exists Output_fk_lg_data")
# @dbHandle.query("create table Output_fk_lg_data as( SELECT * FROM flipkart_lg_data
#   LEFT JOIN fk_seller
#   ON flipkart_lg_data.productcode = fk_seller.productid)")



# #############################################################################################

# @dbHandle.query("drop table if exists Output_flipkart")
# @dbHandle.query("create table Output_flipkart as (
#   SELECT * , (case when ((lowestPrice < .93*flipkart_input.mrp_lg) OR (lowestPrice > mrp_lg) ) then 'Yes' else 'No' end) as price_violation FROM Output_fk_lg_data
#   LEFT JOIN flipkart_input
#   ON Output_fk_lg_data.productcode = flipkart_input.productnumber)")


# #############################################################################

# @dbHandle.query("ALTER TABLE Output_flipkart Add Column mop Float")

# @dbHandle.query("SET SQL_SAFE_UPDATES=0")
# @dbHandle.query("UPDATE Output_flipkart
#   SET remote_url = REPLACE(remote_url, '&affid=nishantfi', '') where remote_url like '%&affid=nishantfi'")
# @dbHandle.query('Update Output_flipkart SET mop = mrp_lg*93/100')
# @dbHandle.query("SET SQL_SAFE_UPDATES=1")


# #*************************************************************************

# results_series = @dbHandle.query("select title,title_portal, strikePrice, lowestPrice, sellername,productcode,remote_url, extimgpath,category,brand,sku,source,available,price_violation,mrp_lg,mop from Output_flipkart ")


# CSV.open("/home/ubuntu/Flipkart/csv/flipkart_lg#{Date.today.to_s}.csv","wb") do |csv|
#   csv << ["Product Name","Title","MRP_Portal", "Price","Seller Name","Product Code", "Link", "Product Image","Category", "Brand","SKU","Portal Name","Availability","Price Violation","MRP_LG","MOP"]
#   results_series.each_hash do |s1|
#     csv << ["#{s1['title']}","#{s1['title_portal']}","#{s1['strikePrice']}", "#{s1['lowestPrice']}", "#{s1['sellername']}","#{s1['productcode']}","#{s1['remote_url']}","#{s1['extimgpath']}","#{s1['category']}","#{s1['brand']}","#{s1['sku']}","#{s1['source']}","#{s1['available']}","#{s1['price_violation']}","#{s1['mrp_lg']}","#{s1['mop']}",]
#   end
# end

# #for sending mail with attachment**************************************************************************************************
# Mail.defaults do
#   delivery_method :smtp,  {
#     :address => 'smtp.gmail.com',
#     :port => '25',
#     :user_name => 'Insights@intellolabs.com',
#     :password => 'intellolabs@123',
#     :enable_starttls_auto => true}
#   end

#   @mail = Mail.new do
#     from 'Insights@intellolabs.com'
#     to 'sujith@comparedapp.com'

#     subject 'Flipkart LG'
#     body 'Hi all
#     Attaching csv file of flipkart lg '
#   end
#   x=Date.today.to_s
#   @mail.add_file("/home/ubuntu/Flipkart/csv/flipkart_lg"+x+".csv")
#   @mail.deliver!

