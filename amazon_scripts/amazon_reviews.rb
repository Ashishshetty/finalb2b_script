require 'pry'
require 'watir'
require 'nokogiri'
require 'mysql'
require 'logger'

AMZlogger = Logger.new( File.join(File.basename(__FILE__) + ".logs"))

AMZlogger.level= Logger::DEBUG

def reviews_curl(productcode,start,searchurl,con,db_out_reviewsput_table, browsername)
	searchurl = "http://www.amazon.in/product-reviews/#{productcode}/ref=cm_cr_arp_d_paging_btm_22?showViewpoints=1&pageNumber=#{start}&sortBy=recent"
	AMZlogger.debug(searchurl)
	retry_count = 5
	# productcode = "B00NMGPGO4"
	con1 = Mysql.new 'localhost', 'root',"root", "nykaa"

	res = con1.query("select count(*) from useragents2")
	res.each_hash do |total|
		@count = total['count(*)'].to_i
	end
	if searchurl == "NA"
		con.query("INSERT INTO #{db_out_reviewsput_table} (title,productcode,reviewername,reviewed_at,review,rating) VALUES ('','#{Mysql.escape_string(productcode)}','','')")
		return
	end
	p searchurl
	url = searchurl
	retrycount = 0
	while retrycount < 3
		begin
			p "hihi"
			id = rand(@count)
			useragentresult = con1.query("select useragent from useragents2 where id = #{id}")
			useragentresult.each_hash do |res|
				@user_agent = res['useragent']
			end
			p @user_agent
			`curl -A "#{@user_agent}" "#{url}" > mobilescource_coke_1.html`
			sleep(5)
			break
		rescue
			puts "hhhh"
			AMZlogger.error("error with useragent #{@user_agent}")
			con1.query("update useragents2 set status = 0 where useragent = '#{@user_agent}' ")
			retrycount = retrycount + 1
			sleep 3
		end
	end

	doc = Nokogiri::HTML(open("mobilescource_coke_1.html"))

	if !doc.title.nil?
	  until !doc.title.nil? and !doc.title.downcase.include? 'robot'
		id = rand(@count)
		useragentresult = con1.query("select useragent from useragents2 where id = #{id}")
		useragentresult.each_hash do |res|
			@user_agent = res['useragent']
		end
		p @user_agent
		`curl -A "#{@user_agent}" "#{url}" > mobilescource_coke_1.html`
		doc = Nokogiri::HTML(open("mobilescource_coke_1.html"))
        sleep 5
        AMZlogger.error("Curl error")
	  end
	end
	con1.close if con1

	if doc.nil?
		AMZlogger.error("Curl Result is null for #{productcode}/ reviews done")
		return
	end

	if doc.css('.review .celwidget').nil?
		title_count =  0
	else
		tile_count = doc.css('.review .celwidget').count 
	end
	puts "here"
	0.upto(tile_count-1) do |i|
		reviewername = doc.css('.review .celwidget .author')[i].text if !doc.css('.review .celwidget .author')[i].nil?
		p review = doc.css('.review .celwidget .review-text')[i].text.tr('^A-za-z0-9 ', '') if !doc.css('.review .celwidget .review-text')[i].nil?
		if !doc.css('.review .celwidget .review-date')[i].nil?
			date = doc.css('.review .celwidget .review-date')[i].text
			date = date.gsub("on ",'') if !date.nil?
			check = date.split(" ").last.to_i
			return if check < 2016
		end
		title = doc.css('.review .celwidget .review-title')[i].text.tr('^A-za-z0-9 ', '') if !doc.css('.review .celwidget .review-title')[i].nil?
		rating = doc.css('.review .celwidget .a-icon-star')[i].text.split[0] if !doc.css('.review .celwidget .a-icon-star')[i].nil?

		p reviewername = "" if reviewername.nil?
		p review = "" if review.nil?
		date = "" if date.nil?
		title = "" if title.nil?
		rating = "" if rating.nil?
		puts productcode
		puts i
		AMZlogger.debug("=======================================================================")

		con.query("INSERT INTO #{db_out_reviewsput_table} (title,productcode,reviewername,reviewed_at,review,rating) VALUES ('#{Mysql.escape_string(title)}','#{Mysql.escape_string(productcode)}','#{Mysql.escape_string(reviewername)}','#{Mysql.escape_string(date)}','#{Mysql.escape_string(review)}','#{Mysql.escape_string(rating)}')")

	end	

	if !doc.css("li.a-last @href")[0].nil?
		start = start + 1
		searchurl = "http://www.amazon.in/" + doc.css("li.a-last @href")[0].value
		retry_count = 3
		while retry_count != 0
			begin
				AMZlogger.debug("url = #{searchurl}")
				reviews_curl(productcode,start,searchurl,con,db_out_reviewsput_table, browsername)
				break
			rescue
				AMZlogger.error("Error with the productcode #{productcode} for reviews with curl retrycount #{retry_count}")
				retry_count -=1
			end
			if retry_count == 0 
				break
			end
		end
			
	end

end




db_user = "root"
db_pass = "root"
database_name =  "loreal"
db_input_table = "amazon_final_input"
db_out_reviewsput_table = "amazon_reviews_loreal"

con = Mysql.new 'localhost', "#{db_user}", "#{db_pass}", "#{database_name}"

con.query("CREATE TABLE IF NOT EXISTS \
	#{db_out_reviewsput_table}(id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, \
	title VARCHAR(2048), \
	productcode VARCHAR(100), \
	reviewername VARCHAR(1024), \
	reviewed_at VARCHAR(1024), 
	rating VARCHAR(10), \
	created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, \
    updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP , \
	review text)")

res = con.query("select distinct productcode from #{db_input_table} where id_amazon > 20 and id_amazon < 100")
if !res.nil? && res.num_rows != 0
	res.each_hash do |row|
		productcode = row["productcode"].strip
		start = 0
		puts "here"
		browsername = "#{db_input_table}_reviewAz"
		searchurl = "http://www.amazon.in/product-reviews/#{productcode}/ref=cm_cr_arp_d_paging_btm_22?showViewpoints=1&pageNumber=#{start}"
		retry_count = 5
		begin 
			reviews_curl(productcode,start,searchurl,con,db_out_reviewsput_table,browsername)
			sleep(2)
		end
	end
end

