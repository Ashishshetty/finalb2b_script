require 'csv'
require 'date'
require 'mysql'
require 'time'
require 'json'
require 'logger'
require 'curb'
require 'nokogiri'
require 'open-uri'
require 'pry'

def pagecurl(productcode)

	url = "http://www.amazon.in/dp/#{productcode}/ref=olp_product_details?"

	retrycount = 0
	
	con1 = Mysql.new 'localhost', 'root',"root", "nykaa"

	res = con1.query("select count(*) from useragents")
	res.each_hash do |total|
		@count = total['count(*)'].to_i
	end

	while retrycount < 3
		begin
			id = rand(@count)
			useragentresult = con1.query("select useragent from useragents where id = #{id}")
			useragentresult.each_hash do |res|
				@user_agent = res['useragent']
			end
			`curl -A "#{@user_agent}" "#{url}" > mobilescource.html`
			sleep(5)
			break
		rescue
			retrycount = retrycount + 1
			sleep 5
		end
	end

	doc = Nokogiri::HTML(open("mobilescource.html"))

	until !doc.title.downcase.include? 'robot'
		id = rand(@count)
		useragentresult = con1.query("select useragent from useragents where id = #{id}")
		useragentresult.each_hash do |res|
			@user_agent = res['useragent']
		end
		`curl -A "#{@user_agent}" "#{url}" > mobilescource.html`
		doc = Nokogiri::HTML(open("mobilescource.html"))
	end

	if !doc.css('#availability').nil?
		availability = doc.css('#availability').text.strip.downcase.include? 'unavailable'
	end

	if !doc.css('#acrCustomerReviewText').nil?
		review_count = doc.css('#acrCustomerReviewText').text.split(' ')[0]
	end

	if !doc.css('#acrPopover')[0].nil?
		rating = doc.css('#acrPopover')[0].text.strip.split(' ')[0]
	end

	if !doc.css('#priceblock_ourprice').empty?
		curl_price = doc.css('#priceblock_ourprice').text.split(' ')[1].gsub(',','').to_f
	end

	if !doc.css('#priceblock_dealprice').empty?
		deal_price = doc.css('#priceblock_dealprice').text.split(' ')[1].gsub(',','').to_f
	end

	if !deal_price.nil? and deal_price > 0
		curl_price = deal_price
	end

	if !doc.css('#merchant-info a')[0].nil?
		curl_seller_name = doc.css('#merchant-info a')[0].text
	end

	if !doc.css('.a-text-strike').empty?
		curl_strikeprice = doc.css('.a-text-strike').text.split(' ')[1].gsub(',','').to_f
	end

	fulfilled_by = doc.css('#ourprice_fbabadge span')[0].values[0].downcase.include? 'swsprite' if !doc.css('#ourprice_fbabadge span')[0].nil?

	if fulfilled_by == nil and !doc.css('#merchant-info').nil?
		fulfilled_by = doc.css('#merchant-info').text.strip.downcase.include? 'fulfilled by amazon'
	end

	if curl_strikeprice.nil?
		curl_strikeprice = curl_price
	end

	return !availability, review_count, rating, fulfilled_by, curl_price, curl_seller_name, curl_strikeprice

end
